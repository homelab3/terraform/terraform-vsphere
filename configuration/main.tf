#################################################################
#                         PROVIDER                              #
#################################################################

provider "vsphere" {
  user                 = var.vsphere_user
  password             = var.vsphere_password
  vsphere_server       = var.vsphere_server
  allow_unverified_ssl = true
}

#################################################################
#                           DATA                                #
#################################################################

data "vsphere_datacenter" "datacenter" {
  name = var.datacenter
}

data "vsphere_compute_cluster" "cluster" {
  name          = var.cluster
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_datastore" "datastore" {
  name          = var.datastore
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_virtual_machine" "template" {
  name          = var.vm_template
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_network" "network" {
  name          = "VM Network"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_host" "host" {
  name          = "p1esxi1a.home"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "template_file" "user-data" {
  template = file("../cloud_init/user_data.cfg")

  vars = {
    HOSTNAME = var.hostname
    IP       = var.vm_ip
  }
}

data "template_file" "network" {
  template = file("../cloud_init/network.cfg")

  vars = {
    IP = var.vm_ip
  }
}

data "template_cloudinit_config" "cloud-init" {
  gzip          = true
  base64_encode = true

  part {
    content_type = "text/cloud-config"
    content      = data.template_file.user-data.rendered
  }

  part {
    content_type = "text/cloud-config"
    content      = data.template_file.network.rendered
  }
}


################################################################# 
#                         RESOURCES                             #
#################################################################

resource "vsphere_virtual_machine" "instance" {
  name             = var.vm_name
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id

  num_cpus  = var.vm_cpu
  memory    = var.vm_ram
  guest_id  = data.vsphere_virtual_machine.template.guest_id
  scsi_type = data.vsphere_virtual_machine.template.scsi_type

  cdrom {
    client_device = true
  }

  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = data.vsphere_virtual_machine.template.network_interface_types[0]
  }
  wait_for_guest_net_timeout = 30

  disk {
    label            = "sda"
    size             = "16"
    thin_provisioned = var.vm_sda_thin_provisioned
  }

  dynamic "disk" {
    for_each = [for d in var.vm_disks : {
      label            = d.label
      size             = d.size
      number           = d.number
      thin_provisioned = coalesce(d.thin_provisioned, false)
    }]

    content {
      label            = disk.value.label
      size             = disk.value.size
      unit_number      = disk.value.number
      eagerly_scrub    = false
      thin_provisioned = disk.value.thin_provisioned
    }
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
    #   linked_clone  = "false"

    # customize {
    #   timeout = "20"
    #   linux_options {
    #     host_name = var.hostname
    #     domain    = "home"
    #   }
    #     network_interface {
    #       ipv4_address = var.vm_ip
    #       ipv4_netmask = 16
    #     }

    #     ipv4_gateway    = "192.168.1.1"
    #     dns_server_list = ["192.168.1.2", "1.1.1.1"]
  }

  vapp {
    properties = {
      user-data = base64gzip(data.template_file.user-data.rendered)
      #network   = base64gzip(data.template_file.network.rendered)
      #user-data = "${base64encode(file("../cloud_init/cloud-init.cfg"))}"
      #hostname  = var.hostname
      #network = "${base64encode(file("../cloud_init/metadata.yml"))}"
    }
  }

  # extra_config = {
  #   "guestinfo.metadata"          = base64encode(file("../cloud_init/metadata.yml"))
  #   "guestinfo.metadata.encoding" = "base64"
  #   "guestinfo.metadata"          = base64encode(file("../cloud_init/user_data.cfg"))
  #   "guestinfo.metadata.encoding" = "base64"
  #   "guestinfo.userdata"          = base64encode(file("../cloud_init/network.cfg"))
  #   "guestinfo.userdata.encoding" = "base64"
  # }
}
