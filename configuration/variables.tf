variable "vsphere_user" {
  type        = string
  description = "vSphere username"
  default     = "administrator@vsphere.local"
}

variable "vsphere_password" {
  type        = string
  description = "vSphere password"
  default     = ""

variable "vsphere_server" {
  type        = string
  description = "vSphere server fQDN"
  default     = "p1vsphere1a.home"
}

variable "vm_location" {
  type        = string
  description = "Terraform provider"
  default     = ""
}

variable "vm_template" {
  type        = string
  description = "Template to use for the creation of the VM"
  default     = ""
}


variable "vm_name" {
  type        = string
  description = "The name of the VM"
  default     = ""
}

variable "hostname" {
  type        = string
  description = "The hostname of the VM"
  default     = ""
}

variable "vm_index" {
  type        = string
  description = "The index of the VM"
  default     = "1a"
}

variable "vm_cpu" {
  type        = number
  description = "The number of CPU"
  default     = 2
}

variable "vm_ram" {
  type        = number
  description = "SThe sze of the RAM in MB"
  default     = 1024
}

variable "vm_network" {
  type        = string
  description = "The name of the network to be used"
  default     = "VM Network"
}

variable "vm_disk_size" {
  type        = number
  description = "The size of first disk in GB"
  default     = 16
}

variable "datacenter" {
  type        = string
  description = "Datacenter name"
  default     = "homelab-datacenter"
}

variable "cluster" {
  type        = string
  description = "cluster name"
  default     = "homelab-cluster"
}

variable "datastore" {
  type        = string
  description = "Datastore name"
  default     = "p1esxi1a-raid5-hdd1"
}

variable "vm_disks" {
  description = "List of the secondary disks"
  type = list(object({
    label            = string
    size             = number
    thin_provisioned = bool
    number           = number
  }))
}

variable "vm_sda_thin_provisioned" {
  type        = bool
  description = "Provisioning of the os disk"
  default     = false
}
variable "vm_ip" {
  type        = string
  description = "The ip of the VM"
  default     = ""
}

variable "vm_type" {
  type        = string
  description = "The type of the VM (ubuntu, centos,...)"
  default     = "ubuntu"
}

