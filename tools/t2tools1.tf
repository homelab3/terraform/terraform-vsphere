module "t2tools1a" {
  source                  = "../configuration"
  datacenter              = "homelab-datacenter"
  cluster                 = "homelab-cluster"
  datastore               = "p1esxi1a-raid5-hdd1"
  vm_template             = "ubuntu20.04-template"
  vm_cpu                  = "2"
  vm_ram                  = "2048"
  vm_name                 = "t2tools1a"
  hostname                = "t2tools1a"
  vm_network              = "VM Network"
  vm_ip                   = "192.168.10.100"
  vm_sda_thin_provisioned = false
  vm_disks = [
    {
      label            = "sdb"
      number           = 2
      size             = 20
      thin_provisioned = true
    },
    {
      label            = "sdc"
      number           = 3
      size             = 20
      thin_provisioned = true
    },
  ]
}
