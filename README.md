# **[DEPRECATED]** Homelab-Terraform-vSphere

Terraform repository for my vSphere homelab.

## Table of Content

<!-- TOC -->

- [Homelab-Terraform-vSphere](#homelab-terraform-vsphere)
    - [Table of Content](#table-of-content)
    - [Introduction and Goals](#introduction-and-goals)
    - [Installation](#installation)
        - [Host](#host)
        - [Remote host](#remote-host)
    - [Architecture](#architecture)
    - [Terraform commands](#terraform-commands)
        - [Initialize project](#initialize-project)
        - [Planification](#planification)
        - [Apply configuration](#apply-configuration)
        - [Destroy](#destroy)
    - [Sources](#sources)
    - [TODO](#todo)

<!-- /TOC -->

## Introduction and Goals

Terraform for my vSphere homelab.

## Installation

### Host

The host for the VMs is an vSphere ESXi 6.5 server.

### Remote host

In order to run the terraform commands from another host, terraform needs to be installed. \
- Download terraform from [Terraform](https://www.terraform.io/downloads.html).

- Unzip to _/usr/local/bin_
    ```zsh
    unzip terraform_XXXX.zip -d /usr/local/bin
    ```
- Check terraform is recognized
    ```bash
    terraform --version
    ```

## Architecture

Here is an example of the current architecture.

```bash
homelab-terraform
├──README.md
├──cloud_init                   <- CloudInit folder the configuration of the network and the base users
│  ├──centos                    <- Configuration per Linux flavor
│  │  ├──network.cfg            <- Network configuration
│  │  └──user_data.cfg          <- User configuration
│  ├──ubuntu
│  │  └──...
│  ...
│
├──common                       <- Common VMs folder
│  ├──c1tools                   <- Project name
│  │  ├──c1tools1.tf            <- Terraform configuration
│  │  └──README.md              <- README of the project
│  ├──c2mariadb1
│  │   └──...
│  ...
│
├──configuration                <- Terraform configuration folder for all VMs
│  ├──variables.tf              <- Variables Terraform file
│  └──main.tf                   <- Main Terraform file
│
├──test                         <- Test VMs folder
│   ├──...
│   ...
│
└──prod                         <- Production VMs folder
    ├──...
    ...
```

The common part contains the VMs which will be used by other VMs. The test part contains all the VM which are in a test stage and do not contain any production data and can therefore be deleted anytimes. The prod part contains all the VM which in production and might therefore contain production data.


## Terraform commands

In order to run terraform commands for a project, we need to go to the project folder.

### Initialize project

Once a project is created it needs to be initalized. This command needs to be run once:

```zsh
terraform init
```

### Planification

To see the changes before applying them :

```zsh
terraform plan
```

### Apply configuration

To apply terraform configuration and therefore create the VM(s) :

```zsh
terraform apply
```

### Destroy

To destroy all VMs of the project :

```zsh
terraform destroy
```

To destroy a specific module in the project :

```zsh
terraform destroy --target=module.MODULE
```

## Sources


## TODO

